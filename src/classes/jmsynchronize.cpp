#include "jmsynchronize.h"

QString jmSynchronize::getServer() const
{
    return server;
}

void jmSynchronize::setServer(const QString &value)
{
    server = value;
}

jmSynchronize::jmSynchronize(jmDatabase *db, QObject *parent) :
    QObject(parent)
{
    this->jmdb = db;
    this->dm = new DownloadManager();
    this->server = "";

    QObject::connect(dm, SIGNAL(finished()),
                     this, SLOT(downloadFinish()));
}

void jmSynchronize::synchro(void) {
    QDateTime lastSynchro = jmdb->getLastSynchro();

    // QUrl url("http://" + this->server + "/jmquiz/get_questions.php?last_update=" + lastSynchro.toString("yyyy-MM-dd hh:mm:ss"));
    QUrl url("http://just-modeste.hd.free.fr:8080/getquestions?languageid=1&lastsynchro=" + lastSynchro.toString("yyyy-MM-dd+hh:mm:ss"));
    this->dm->doDownload(url, "questions.json");
}

void jmSynchronize::downloadFinish() {

    QFile loadFile("questions.json");

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));

    if (loadDoc.isObject()) {
        QJsonArray obj = loadDoc.object()["questions"].toArray();

        for (int i = 0; i < obj.count(); i++) {
            QJsonObject iter = obj[i].toObject();

            JMQuestion *question = new JMQuestion();

            question->setIdent(iter["QST_ID"].toInt());
            question->setLabel(iter["QST_LABEL"].toString());
            question->setIdCateg(iter["CAT_ID"].toInt());
            question->setIdForce(iter["FOR_ID"].toInt());

            question->answer1()->setIdent(iter["ANS_ID_1"].toInt());
            question->answer1()->setLabel(iter["ANS_LABEL_1"].toString());
            question->answer1()->setCorrect((bool)(iter["CORRECT_1"].toInt()));

            question->answer2()->setIdent(iter["ANS_ID_2"].toInt());
            question->answer2()->setLabel(iter["ANS_LABEL_2"].toString());
            question->answer2()->setCorrect((bool)(iter["CORRECT_2"].toInt()));

            question->answer3()->setIdent(iter["ANS_ID_3"].toInt());
            question->answer3()->setLabel(iter["ANS_LABEL_3"].toString());
            question->answer3()->setCorrect((bool)(iter["CORRECT_3"].toInt()));

            question->answer4()->setIdent(iter["ANS_ID_4"].toInt());
            question->answer4()->setLabel(iter["ANS_LABEL_4"].toString());
            question->answer4()->setCorrect((bool)(iter["CORRECT_4"].toInt()));

            question->updateDb();
        }       
    }
    this->jmdb->setLastSynchro();
    emit this->synChronizeFinished();
}
