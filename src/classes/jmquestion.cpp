#include "jmquestion.h"

JMQuestion::JMQuestion(QObject *parent) :
    QObject(parent)
{
//    qsrand(QTime::currentTime().msec());
    this->_ident = -1;
    this->_label = "";
    for (int i = 0; i < 4; i++) {
        this->_answers[i] = new JMAnswer(parent);
    }
    this->_idCateg = -1;
    this->_idForce = -1;
}

int JMQuestion::ident() {
    return this->_ident;
}

void JMQuestion::setIdent(int val) {
    this->_ident = val;
    emit this->identChanged();
}

QString JMQuestion::label() {
    return this->_label;
}

void JMQuestion::setLabel(QString val) {
    this->_label = val;
    emit labelChanged();
}

JMAnswer* JMQuestion::answer1() {
    return this->_answers[0];
}

void JMQuestion::setAnswer1(JMAnswer *val) {
    this->_answers[0] = val;
    emit answer1Changed();
}

JMAnswer* JMQuestion::answer2() {
    return this->_answers[1];
}

void JMQuestion::setAnswer2(JMAnswer *val) {
    this->_answers[1] = val;
    emit answer2Changed();
}

JMAnswer* JMQuestion::answer3() {
    return this->_answers[2];
}

void JMQuestion::setAnswer3(JMAnswer *val) {
    this->_answers[2] = val;
    emit answer3Changed();
}

JMAnswer* JMQuestion::answer4() {
    return this->_answers[3];
}

void JMQuestion::setAnswer4(JMAnswer *val) {
    this->_answers[3] = val;
    emit answer4Changed();
}

int JMQuestion::idCateg() {
    return this->_idCateg;
}

void JMQuestion::setIdCateg(int val) {
    this->_idCateg = val;
    emit idCategChanged();
}

int JMQuestion::idForce() {
    return this->_idForce;
}

void JMQuestion::setIdForce(int val) {
    this->_idForce = val;
    emit idForceChanged();
}

bool JMQuestion::updateDb(void) {
    bool    ret = false;

    if (this->label() != "") {
        QString sql =
                "insert or replace into QST_QUESTIONS "
                "(QST_ID, QST_LABEL, CAT_ID, FOR_ID) "
                "values (:id, :label, :idcat, :idforce) "
                ;
        QSqlQuery   query;

        if (query.prepare(sql)) {
            query.bindValue(":id", this->_ident);
            query.bindValue(":label", this->_label);
            query.bindValue(":idcat", this->_idCateg);
            query.bindValue(":idforce", this->_idForce);
            ret = query.exec();
        }
    }
    if (ret) {
        ret = updateAnswers();
    }
    if (ret) {
        ret = updateLinks();
    }
    return ret;
}

bool JMQuestion::updateAnswers() {
    bool    ret = true;

    for (int i = 0; i < 4 && ret; i++) {
        ret = this->_answers[i]->updateDb();
    }
    return ret;
}

bool JMQuestion::updateLinks() {
    bool     ret = true;
    QString sql =
            "insert or replace into QST_ANS "
            "(QST_ID, ANS_ID, ANS_CORRECT) "
            "values (:idqst, :idans, :correct) "
            ;

    QSqlQuery query;
    if (query.prepare(sql)) {
        for (int i = 0; i < 4 && ret; i++) {
            query.bindValue(":idqst", this->_ident);
            query.bindValue(":idans", this->_answers[i]->ident());
            query.bindValue(":correct", this->_answers[i]->isCorrect());

            ret = query.exec();
        }
    }
    return ret;
}

bool JMQuestion::loadQuestion(int id) {
    QString sql =
            "select qst.QST_ID "
                ", qst.QST_LABEL "
                ", qst.CAT_ID "
                ", qst.FOR_ID "
                ", rps1.ANS_ID as ANS_ID_1 "
                ", rps1.ANS_LABEL as ANS_LABEL_1 "
                ", qr1.ANS_CORRECT as CORRECT_1 "
                ", rps2.ANS_ID as ANS_ID_2 "
                ", rps2.ANS_LABEL as ANS_LABEL_2 "
                ", qr2.ANS_CORRECT as CORRECT_2 "
                ", rps3.ANS_ID as ANS_ID_3 "
                ", rps3.ANS_LABEL as ANS_LABEL_3 "
                ", qr3.ANS_CORRECT as CORRECT_3 "
                ", rps4.ANS_ID as ANS_ID_4 "
                ", rps4.ANS_LABEL as ANS_LABEL_4 "
                ", qr4.ANS_CORRECT as CORRECT_4 "
            "from QST_QUESTIONS qst "
            "inner join QST_ANS qr1 on (qr1.QST_ID = qst.QST_ID) "
            "inner join ANS_ANSWERS rps1 on (rps1.ANS_ID = qr1.ANS_ID) "

            "inner join QST_ANS qr2 on (qr2.QST_ID = qst.QST_ID and qr2.ANS_ID > qr1.ANS_ID) "
            "inner join ANS_ANSWERS rps2 on (rps2.ANS_ID = qr2.ANS_ID) "
            "inner join QST_ANS qr3 on (qr3.QST_ID = qst.QST_ID and qr3.ANS_ID > qr2.ANS_ID) "
            "inner join ANS_ANSWERS rps3 on (rps3.ANS_ID = qr3.ANS_ID) "
            "inner join QST_ANS qr4 on (qr4.QST_ID = qst.QST_ID and qr4.ANS_ID > qr3.ANS_ID) "
            "inner join ANS_ANSWERS rps4 on (rps4.ANS_ID = qr4.ANS_ID) "
            "where qst.QST_ID = :id "
            ;
    QSqlQuery   query;

    int count = countQuestions();
    // choix aléatoire de la question
    id =  QRandomGenerator::global()->bounded(count - 1) + 1;

    // on vérifie qu'on n'a pas épuiser toutes les questions
    if (this->_listQuestion.count() == count) {
        return false;
    }
    // On vérifie qu'on n'a pas déjà utiliser la question
    while (this->_listQuestion.contains(id) == true) {
        // On prends la 1er question suivante non utilisée
        id++;
        if (id > count) {
            id = 1;
        }
    }
    this->_listQuestion.append(id);

    if (query.prepare(sql)) {
        query.bindValue(":id", id);
        if (query.exec()) {
            if (query.next()) {
                this->setIdent(query.value("QST_ID").toInt());
                this->setLabel(query.value("QST_LABEL").toString());

                bool tab[4];
                for (int i = 0; i < 4; i++) {
                    tab[i] = false;
                }

                for (int i = 0; i < 4; i++) {
                    int val = -1;
                    do {
                        val = QRandomGenerator::global()->bounded(4) % 4;
                    }
                    while (tab[val]);
                    tab[val] = true;
                    int id = -1;
                    QString lab = "";
                    bool    cor = false;

                    switch (val) {
                    case 0 :
                        id = query.value("ANS_ID_1").toInt();
                        lab = query.value("ANS_LABEL_1").toString();
                        cor = query.value("CORRECT_1").toBool();
                        break;
                    case 1 :
                        id = query.value("ANS_ID_2").toInt();
                        lab = query.value("ANS_LABEL_2").toString();
                        cor = query.value("CORRECT_2").toBool();
                        break;
                    case 2 :
                        id = query.value("ANS_ID_3").toInt();
                        lab = query.value("ANS_LABEL_3").toString();
                        cor = query.value("CORRECT_3").toBool();
                        break;
                    case 3 :
                        id = query.value("ANS_ID_4").toInt();
                        lab = query.value("ANS_LABEL_4").toString();
                        cor = query.value("CORRECT_4").toBool();
                        break;
                    }

                    this->_answers[i]->setIdent(id);
                    this->_answers[i]->setLabel(lab);
                    this->_answers[i]->setCorrect(cor);
                }
                return true;
            }
        }
    }
    return false;
}

int JMQuestion::countQuestions() {
    int     ret = 0;
    QString sql =
            "select count(*) as COUNT_QUESTIONS "
            "from QST_QUESTIONS "
            ;
    QSqlQuery   query;

    if (query.prepare(sql)) {
        if (query.exec()) {
            if (query.next()) {
                ret = query.value("COUNT_QUESTIONS").toInt();
            }
        }
    }
    return ret;
}
