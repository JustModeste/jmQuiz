#ifndef JMGAME_H
#define JMGAME_H

#include <QObject>

class JMGame : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int score READ score WRITE setScore NOTIFY scoreChanged)
    Q_PROPERTY(int bet READ bet WRITE setBet NOTIFY betChanged)

    int _score;
    int _bet;
public:
    explicit JMGame(QObject *parent = 0);

    int score();
    void setScore(int val);

    int bet();
    void setBet(int val);

signals:
    void scoreChanged();
    void betChanged();

public slots:

};

#endif // JMGAME_H
