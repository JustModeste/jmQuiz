#include "jmgame.h"

JMGame::JMGame(QObject *parent) :
    QObject(parent)
{
    this->_score = 5;
    this->_bet = 0;
}

int JMGame::score() {
    return this->_score;
}

void JMGame::setScore(int val) {
    this->_score = val;
    emit this->scoreChanged();
}

int JMGame::bet() {
    return this->_bet;
}

void JMGame::setBet(int val) {
    this->_bet = val;
    emit this->betChanged();
}

