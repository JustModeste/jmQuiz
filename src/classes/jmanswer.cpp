#include "jmanswer.h"

JMAnswer::JMAnswer(QObject *parent) :
    QObject(parent)
{
    this->_ident = -1;
    this->_label = "";
    this->_correct = false;
}

int JMAnswer::ident() {
    return this->_ident;
}

void JMAnswer::setIdent(int val) {
    this->_ident = val;
    this->_correct = false;
    emit this->identChanged();
}

QString JMAnswer::label() {
    return this->_label;
}

void JMAnswer::setLabel(QString val) {
    this->_label = val;
    emit labelChanged();
}

bool JMAnswer::isCorrect() {
    return this->_correct;
}

void JMAnswer::setCorrect(bool val) {
    this->_correct = val;
    emit correctChanged();
}

bool JMAnswer::updateDb(void) {
    bool    ret = false;

    if (this->label() != "") {
        QString sql =
                "insert or replace into ANS_ANSWERS "
                "(ANS_ID, ANS_LABEL) "
                "values (:id, :label) "
                ;
        QSqlQuery   query;

        if (sql != "" && query.prepare(sql)) {
            query.bindValue(":id", this->_ident);
            query.bindValue(":label", this->_label);
            if (query.exec()) {
                ret = true;
            }
        }
    }
    return ret;
}
