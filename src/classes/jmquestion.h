#ifndef JMQUESTION_H
#define JMQUESTION_H

#include <QObject>
#include <QtSql>
#include <QSqlQuery>
#include <QList>
#include <QRandomGenerator>

#include "jmanswer.h"

class JMQuestion : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int ident READ ident WRITE setIdent NOTIFY identChanged)
    Q_PROPERTY(QString label READ label WRITE setLabel NOTIFY labelChanged)
    Q_PROPERTY(JMAnswer* answer1 READ answer1 WRITE setAnswer1 NOTIFY answer1Changed)
    Q_PROPERTY(JMAnswer* answer2 READ answer2 WRITE setAnswer2 NOTIFY answer2Changed)
    Q_PROPERTY(JMAnswer* answer3 READ answer3 WRITE setAnswer3 NOTIFY answer3Changed)
    Q_PROPERTY(JMAnswer* answer4 READ answer4 WRITE setAnswer4 NOTIFY answer4Changed)

    Q_PROPERTY(int idCateg READ idCateg WRITE setIdCateg NOTIFY idCategChanged)
    Q_PROPERTY(int idForce READ idForce WRITE setIdForce NOTIFY idForceChanged)

    int         _ident;
    QString     _label;
    JMAnswer*   _answers[4];
    int         _idCateg;
    int         _idForce;
    QList<int>  _listQuestion;

    bool    updateAnswers();
    bool    updateLinks();

public:
    explicit JMQuestion(QObject *parent = 0);

    int ident();
    void setIdent(int val);

    QString label();
    void setLabel(QString val);

    JMAnswer* answer1();
    void setAnswer1(JMAnswer* val);

    JMAnswer* answer2();
    void setAnswer2(JMAnswer* val);

    JMAnswer* answer3();
    void setAnswer3(JMAnswer* val);

    JMAnswer* answer4();
    void setAnswer4(JMAnswer* val);

    int idCateg();
    void setIdCateg(int val);

    int idForce();
    void setIdForce(int val);

    bool updateDb();

    Q_INVOKABLE bool loadQuestion(int id);
    Q_INVOKABLE int countQuestions();

signals:
    void identChanged();
    void labelChanged();
    void answer1Changed();
    void answer2Changed();
    void answer3Changed();
    void answer4Changed();
    void idCategChanged();
    void idForceChanged();

public slots:

};

#endif // JMQUESTION_H
