#ifndef JMSYNCHRONIZE_H
#define JMSYNCHRONIZE_H

#include <QObject>
#include <QUrl>
#include <QJsonDocument>
#include <QString>

#include "downloadmanager.h"
#include "../../jmDatabase/Classes/jmdatabase.h"
#include "jmquestion.h"

class jmSynchronize : public QObject
{
    Q_OBJECT

private:
    DownloadManager *dm;
    jmDatabase      *jmdb;
    QString         server;
public:
    explicit jmSynchronize(jmDatabase *db, QObject *parent = 0);

    Q_INVOKABLE void synchro(void);

    Q_INVOKABLE QString getServer() const;

    Q_INVOKABLE void setServer(const QString &value);

signals:
    void    synChronizeFinished();
public slots:
    void    downloadFinish();

};

#endif // JMSYNCHRONIZE_H
