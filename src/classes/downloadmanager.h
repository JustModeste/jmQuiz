#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QSslError>
#include <QFileInfo>

class DownloadManager: public QObject
{
    Q_OBJECT
    QNetworkAccessManager manager;
    QList<QNetworkReply *> currentDownloads;

    QString fileName;

public:
    DownloadManager();
    void doDownload(const QUrl &url, const QString file);
    bool saveToDisk(const QString &filename, QIODevice *data);

public slots:
    void downloadFinished(QNetworkReply *reply);
    void sslErrors(const QList<QSslError> &errors);

signals:
    void finished();
};

#endif // DOWNLOADMANAGER_H
