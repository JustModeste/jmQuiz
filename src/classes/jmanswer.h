#ifndef JMANSWER_H
#define JMANSWER_H

#include <QObject>
#include <QtSql>
#include <QSqlQuery>

class JMAnswer : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int ident READ ident WRITE setIdent NOTIFY identChanged)
    Q_PROPERTY(QString label READ label WRITE setLabel NOTIFY labelChanged)
    Q_PROPERTY(bool correct READ isCorrect WRITE setCorrect NOTIFY correctChanged)

    int     _ident;
    QString _label;
    bool    _correct;
public:
    explicit JMAnswer(QObject *parent = 0);

    int ident();
    void setIdent(int val);

    QString label();
    void setLabel(QString val);

    bool isCorrect();
    void setCorrect(bool val);

    bool updateDb();

signals:
    void identChanged();
    void labelChanged();
    void correctChanged();

public slots:

};

#endif // JMANSWER_H
