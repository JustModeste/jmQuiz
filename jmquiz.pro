TEMPLATE = app

QT += qml quick sql

SOURCES += main.cpp \
    jmDatabase/Classes/jmdatabase.cpp \
    jmDatabase/Classes/jmfilequery.cpp \
    src/classes/jmsynchronize.cpp \
    src/classes/downloadmanager.cpp \
    src/classes/jmquestion.cpp \
    src/classes/jmanswer.cpp \
    src/classes/jmgame.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    jmDatabase/Classes/jmdatabase.h \
    jmDatabase/Classes/jmfilequery.h \
    src/classes/jmsynchronize.h \
    src/classes/downloadmanager.h \
    src/classes/jmquestion.h \
    src/classes/jmanswer.h \
    src/classes/jmgame.h

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle.properties \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml

contains(ANDROID_TARGET_ARCH,arm64-v8a) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
}
