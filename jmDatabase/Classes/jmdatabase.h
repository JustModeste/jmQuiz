#ifndef JMDATABASE_H
#define JMDATABASE_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDirIterator>
#include <QString>
#include <QDateTime>

#include <QDebug>

#include "jmfilequery.h"

class jmDatabase
{
private:
    QSqlDatabase _db;
    bool		 _isOpen;
    QString		 _server;
    QString		 _dbname;
    QString		 _username;
    QString		 _userpass;
    int          _lastIdSynchro;
public:
    jmDatabase(QString server, QString dbname, QString username, QString userpass);
    ~jmDatabase(void);

    bool	open(void);
    bool	isOpen(void);
    bool	update(void);

    QDateTime getLastSynchro(void);
    bool    setLastSynchro(void);
};

#endif // JMDATABASE_H
