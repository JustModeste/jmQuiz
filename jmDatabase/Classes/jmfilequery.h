#ifndef JMFILEQUERY_H
#define JMFILEQUERY_H

#include <QString>
#include <QFile>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QTextStream>

#include <QDebug>

class jmFileQuery
{
public:
    jmFileQuery(QSqlDatabase db, QString filename);
    bool    executeSql();
private:
    QString fileName;
    QSqlDatabase db;

    bool ExecuteQuery(QString sql);
};

#endif // JMFILEQUERY_H
