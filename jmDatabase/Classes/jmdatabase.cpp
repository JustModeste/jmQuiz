#include "jmdatabase.h"

jmDatabase::jmDatabase(QString server, QString dbname, QString username, QString userpass)
{
    qDebug() << "test";
    this->_db = QSqlDatabase::addDatabase("QSQLITE");
    this->_server = server;
    this->_dbname = dbname;
    this->_username = username;
    this->_userpass = userpass;
    this->_isOpen = false;
}

jmDatabase::~jmDatabase()
{
   if (this->_isOpen) {
      this->_db.close();
   }
}

bool jmDatabase::open()
{
   this->_db.setHostName(this->_server);
   this->_db.setDatabaseName(this->_dbname);
   this->_db.setUserName(this->_username);
   this->_db.setPassword(this->_userpass);
   this->_isOpen = this->_db.open();

   if (this->_isOpen) {
      qDebug() << "Database : '" << this->_dbname << "' is Open ;-)\n";
      this->_isOpen = this->update();
   } else {
      qDebug() << "Unable to open database '" << this->_dbname << " ;-(\n";
   }
   return this->_isOpen;
}

bool jmDatabase::isOpen() {
    return this->_isOpen;
}

bool jmDatabase::update()
{
   bool        ret = true;
   QSqlQuery query;

   qDebug() << "jmDatabase::Update()\n";
   // Récupération de la liste des fichiers SQL
   QDirIterator it(":sql", QDirIterator::Subdirectories);
   while (it.hasNext()) {
      QString filename = it.next();
      qDebug() << filename;
      bool    toUpdate = true;
      QString sql =
            "SELECT UPD_ID "
            "FROM UPD_UPDATES "
            "WHERE UPD_SCRIPT = :script";

      if (query.prepare(sql)) {
         query.bindValue(":script", filename);
         if (query.exec()) {
            if (query.next()) {
               toUpdate = false;
            }
         }
      }
      qDebug() << toUpdate;
      if (toUpdate) {
         jmFileQuery *fileQuery = new jmFileQuery(this->_db, filename);

         if (fileQuery->executeSql()) {
            QString sql =
                  "INSERT INTO UPD_UPDATES "
                  "(UPD_SCRIPT, UPD_DATE) "
                  "VALUES (:script, datetime(current_timestamp, 'localtime')) "
                  ;

            if (query.prepare(sql)) {
               query.bindValue(":script", filename);
               if (!query.exec()) {
                  ret = false;
               }
            }
         }
      }
   }
   return ret;
}

QDateTime jmDatabase::getLastSynchro(void) {
    QDateTime ret(QDate(2014,01,01), QTime(0,0,0,0));

    this->_lastIdSynchro = 0;

    QString sql =
            "select SYC_ID "
                ", SYC_DATE "
            "from SYC_SYNCHROS "
            "order by SYC_DATE desc "
            "limit 1"
            ;
    QSqlQuery query;

    if (query.prepare(sql)) {
        if (query.exec()) {
            if (query.next()) {
                ret = query.value("SYC_DATE").toDateTime();
                this->_lastIdSynchro = query.value("SYC_ID").toInt();
            }
        }
    }
    return ret;

}

bool    jmDatabase::setLastSynchro(void) {
    bool    ret = true;
    QString sql =
            "INSERT INTO SYC_SYNCHROS "
            "(SYC_ID, SYC_DATE) "
            "VALUES (:id, current_timestamp) "
            ;
    QSqlQuery query;

    if (query.prepare(sql)) {
        query.bindValue(":id", ++this->_lastIdSynchro);
        if (!query.exec()) {
            ret = false;
        }
    }
    return ret;
}
