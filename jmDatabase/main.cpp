#include <QCoreApplication>
#include <QDebug>

#include "Classes/jmdatabase.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    qDebug() << "jmDatabase's test !\n";

    jmDatabase *jmdb = new jmDatabase("jmdatabase.db", "jmdatabase", "jmdatabase");

    if (jmdb->IsOpen()) {
        jmdb->Update();
    }
    return a.exec();
}
