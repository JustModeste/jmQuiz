#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <qqml.h>

#include "jmDatabase/Classes/jmdatabase.h"
#include "src/classes/jmsynchronize.h"

#include "src/classes/jmquestion.h"
//#include "src/classes/jmanswer.h"

#include "src/classes/jmgame.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    jmDatabase *jmdb = new jmDatabase(
                "localhost",
                "jmquiz.db",
                "jmquiz",
                "jmquiz");

    jmdb->open();

    jmSynchronize jmSynchro(jmdb);
    jmSynchro.setServer("just.modeste11.free.fr/");


//    Game    myGame;
    QQmlApplicationEngine engine;

    JMQuestion* oQuestion = new JMQuestion();

    qmlRegisterType<JMAnswer>("jmquiz", 1, 0, "JMAnswer");

    QQmlContext *context = engine.rootContext();

    JMGame* oGame = new JMGame();

    context->setContextProperty("oSynchro", &jmSynchro);
    context->setContextProperty("oQuestion", oQuestion);
    context->setContextProperty("oGame", oGame);

    engine.load(QUrl(QStringLiteral("qrc:///qml/main.qml")));

    return app.exec();
}
