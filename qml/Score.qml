import QtQuick 2.0

Rectangle {
    id: myRoot
    width: 480
    height: 50

    Row {
        JMItem {
            width: myRoot.width / 3
            color: "#4a90c7"
            text: qsTr("Score")
            topColor: "#4a90c7"
            bottomColor: "#000000"
            textColor: "#ffffff"
            radius: 0
            border.width: 0
        }
        JMItem {
            width: myRoot.width / 6
            text: oGame.score
            topColor: "#4a90c7"
            bottomColor: "#000000"
//            textColor: "#ffffff"
            radius: 0
            border.width: 0
        }
        JMItem {
            width: myRoot.width / 3
            text: qsTr("Mise")
            topColor: "#4a90c7"
            bottomColor: "#000000"
            textColor: "#ffffff"
            radius: 0
            border.width: 0
        }
        JMItem {
            width: myRoot.width / 6
            text: oGame.bet
            topColor: "#4a90c7"
            bottomColor: "#000000"
//            textColor: "#ffffff"
            radius: 0
            border.width: 0
        }
    }
}
