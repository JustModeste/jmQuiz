import QtQuick 2.0

Item {
    id: myRoot
    width: 480
    height: 590

    property int widthElem: myRoot.width - 30

    signal cancel()

    JMItem {
        id: iTitle
        text: qsTr("Règles du jeu")
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 5
        width: myRoot.width
        height: myRoot.height / 10
    }
    Text {
        wrapMode: Text.WordWrap
        text: qsTr("Obtenir le plus gros score\nVous disposez d'une mise de 5 points pour commencer\nVous misez 1, 3 ou 5 points si votre mise restante le permet\nVous répondez à la question posée\nVous gagnez 2x la mise en cas de bonne réponse\nVous perdez votre mise en cas d'erreur\nLa partie continue tant qu'il vous reste des points de mise")
        anchors.bottomMargin: 10
        anchors.topMargin: 10
        anchors.top: iTitle.bottom
        anchors.bottom: myBottom.bottom
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10
    }
    BottomBar {
        id: myBottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        onCancel: myRoot.cancel();
    }    
}
