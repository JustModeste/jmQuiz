import QtQuick 2.0
import QtQuick.Layouts 1.1

Item {
    property int margin: 10

    property alias question: myQuestion.text
    property alias answer1: myFirst.text
    property alias answer2: mySecond.text
    property alias answer3: myThird.text
    property alias answer4: myFour.text

    id: myRoot
    width: 480
    height: 590

    signal result()

    JMItem {
        id: myQuestion
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        height: (myRoot.height - 2 * margin) / 2
        color: "#bdac23"
    }
    JMButton {
        id: myFirst
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: (myRoot.width + margin) / 2
        anchors.top: parent.top
        anchors.topMargin: myQuestion.height + margin
        height: (myRoot.height - 2 * margin) / 4

         mouseArea.onClicked: verifValue(oQuestion.answer1.correct)
    }
    JMButton {
        id: mySecond
        anchors.left: parent.left
        anchors.leftMargin: (myRoot.width + margin) / 2
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.top: parent.top
        anchors.topMargin: myQuestion.height + margin
        height: (myRoot.height - 2 * margin) / 4

         mouseArea.onClicked: verifValue(oQuestion.answer2.correct)
    }
    JMButton {
        id: myThird
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: (myRoot.width + margin) / 2
        anchors.top: parent.top
        anchors.topMargin: myQuestion.height + myFirst.height + 2 * margin
        height: (myRoot.height - 2 * margin) / 4

         mouseArea.onClicked: verifValue(oQuestion.answer3.correct)
    }
    JMButton {
        id: myFour
        anchors.left: parent.left
        anchors.leftMargin: (myRoot.width + margin) / 2
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.top: parent.top
        anchors.topMargin: myQuestion.height + myFirst.height + 2 * margin
        height: (myRoot.height - 2 * margin) / 4

         mouseArea.onClicked: verifValue(oQuestion.answer4.correct)
    }
    JMButton {
        id: myResult
        width: parent.width / 3 * 2
        height: (myRoot.height - 2 * margin) / 4
        border.width: 1
        radius: 20
        visible: false
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

         mouseArea.onClicked: {
            myRoot.state = "";
            result()
        }
    }

    function verifValue(val) {
        var result = qsTr("ERREUR");
        var bottomColor = "#ff0000";
        var textColor = "#ffffff";

        if (val === true) {
            result = qsTr("CORRECT");
            bottomColor = "#00ff44";
            textColor = "#000000";
            oGame.score += (oGame.bet * 2);
        }
        /*
        else {
            oGame.score -= oGame.bet
        }
        */
        myResult.text = result;
        myResult.bottomColor = bottomColor;
        myResult.textColor = textColor;
        myResult.border.color = bottomColor;
        myRoot.state = "State1";
    }

    states: [
        State {
            name: "State1"

            PropertyChanges {
                target: myResult
                visible: true
            }

            PropertyChanges {
                target: myFirst
                visible: true
            }

            PropertyChanges {
                target: myFirst
                enabled: false
            }

            PropertyChanges {
                target: mySecond
                enabled: false
            }
            PropertyChanges {
                target: myThird
                enabled: false
            }
            PropertyChanges {
                target: myFour
                enabled: false
            }
        }
    ]
}
