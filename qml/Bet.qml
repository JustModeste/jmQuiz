import QtQuick 2.0

Item {
    id: myRoot
    width: 480
    height: 590

    signal cancel()
    signal choice(int value)

    onVisibleChanged: {
        if (myRoot.visible) {
            console.log("mise visible");
            b5Points.visible = true;
            b3Points.visible = true;
            b1Point.visible = true;
            bEndGame.visible = false;

            if (oGame.score < 5) {
                b5Points.visible = false;
            }
            if (oGame.score < 3) {
                b3Points.visible = false;
            }
            if (oGame.score < 1) {
                b1Point.visible = false;
                bEndGame.visible = true;
            }
        }
    }

    JMItem {
        id: iTitle
        text: qsTr("Mise")
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 5
        width: myRoot.width
        height: myRoot.height / 10
    }

    Column {
        id: colunm1
        spacing: myRoot.height / 30
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        JMButton {
            id: b1Point
            width: myRoot.width - 2 * colunm1.spacing
            height: myRoot.height / 6
            text: qsTr("1 point");
             mouseArea.onClicked: myRoot.choice(1);
        }

        JMButton {
            id: b3Points
            width: myRoot.width - 2 * colunm1.spacing
            height: myRoot.height / 6
            text: qsTr("3 points");
             mouseArea.onClicked: myRoot.choice(3);
        }

        JMButton {
            id: b5Points
            width: myRoot.width - 2 * colunm1.spacing
            height: myRoot.height / 6
            text: qsTr("5 points");
             mouseArea.onClicked: myRoot.choice(5);
        }
        JMButton {
            id: bEndGame
            width: myRoot.width - 2 * colunm1.spacing
            height: myRoot.height / 3
            text: qsTr("Fin de partie");
            visible: false

             mouseArea.onClicked: myRoot.cancel();
        }
    }
    BottomBar {
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        onCancel: myRoot.cancel();
    }
}
