import QtQuick 2.5
import QtQuick.Controls 2.0

Item {
    id: myRoot
    width: 480
    height: 590

    property int widthElem: myRoot.width - 30

    signal cancel()

    JMItem {
        id: iTitle
        text: qsTr("Paramètres")
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 5
        width: myRoot.width
        height: myRoot.height / 10
    }
    Column {
       id: column1
       anchors.rightMargin: 0
       anchors.leftMargin: 0
       anchors.top: iTitle.bottom
       anchors.topMargin: 49
       spacing: 5
       anchors.bottomMargin: 6
       anchors.right: parent.right
       anchors.bottom: myBottom.top
       anchors.left: parent.left
       Text {
          text: "Serveur de questions :"
          anchors.right: parent.right
          anchors.left: parent.left
          anchors.leftMargin: 0
          horizontalAlignment: Text.AlignHCenter

       }


       TextField {
          id: textfield
          text: qsTr("192.168.1.141")
          transformOrigin: Item.Center
          horizontalAlignment: Text.AlignHCenter
          anchors.horizontalCenter: parent.horizontalCenter

       }
    }

    BottomBar {
        id: myBottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        onCancel: myRoot.cancel();
    }
}

