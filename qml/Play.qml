import QtQuick 2.0

Item {
    id: myRoot
    width: 480
    height: 590

    signal cancel();

    Bet {
        id:myBet
        anchors.fill: parent

        onCancel: myRoot.cancel()
        onChoice: {
            var question = oQuestion.loadQuestion(1);
            if (question == false) {
                myRoot.state = "";
                myRoot.cancel();
            } else {
               myQuestion.question = oQuestion.label;
               myQuestion.answer1 = oQuestion.answer1.label;
               myQuestion.answer2 = oQuestion.answer2.label;
               myQuestion.answer3 = oQuestion.answer3.label;
               myQuestion.answer4 = oQuestion.answer4.label;

               myRoot.state = "Question";
               oGame.bet = value;
               oGame.score -= value;
            }
        }
    }

    Question {
        id: myQuestion
        anchors.fill: parent
        visible: false

        onResult: myRoot.state = ""
    }
    states: [
        State {
            name: "Question"
            PropertyChanges {
                target: myBet
                visible: false
            }
            PropertyChanges {
                target: myQuestion
                visible: true
            }
        }
    ]
}
