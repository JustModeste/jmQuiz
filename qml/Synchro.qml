import QtQuick 2.0

Item {
    id: myRoot
    width: 480
    height: 590

    onVisibleChanged: {
        if (myRoot.visible) {
            oSynchro.synchro();
            myTimer.running = true;
        }
    }

    Timer {
        id: myTimer
        interval: 1000;
        running: false;
        repeat: false;
        onTriggered: {
            myTimer.running = false;
            console.log(Date().toString());
            myRoot.cancel();
        }
    }
    signal cancel()

    Column {
        id: colunm1
        spacing: myRoot.height / 30
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        JMItem {
            id: iSynchro
            width: myRoot.width - 2 * colunm1.spacing
            height: myRoot.height / 6
            text: qsTr("Synchronisation en cours");
        }
    }
    BottomBar {
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        onCancel: myRoot.cancel();

    }


}
