import QtQuick 2.2
import QtQuick.Window 2.1

Window {
    id: myWindow
    visible: true
    width: 480
    height: 640

    onVisibleChanged: {
        if (myWindow.visible && oQuestion.countQuestions() === 0) {
            myRoot.state = "Synchro";
        console.log("départ");
        }
    }

    Item {
        id: myRoot
        anchors.fill: parent
        visible: true
        Score {
            id: myScore
            height: 50
            anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.top: parent.top
            anchors.topMargin: 5
        }
        Start {

            id: myStart
            visible: true
            anchors.top: myScore.bottom
            anchors.topMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.right: parent.right
            anchors.rightMargin: 5

            onChoice: {
                switch (value) {
                case -1 :    // Quit
                    Qt.quit();
                    break;
                case 1 :    // Rules
                    myRoot.state = "Rules";
                    break;
                case 2 :    // Synchro
                    myRoot.state = "Synchro";
                    //oSynchro.synchro();

                    break;
                case 3 :    // Play
                    oGame.bet = 0;
                    oGame.score = 5;
                    myRoot.state = "Play";
                    break;
                case 4 :   // Parameters
                    myRoot.state = "Parameters";
                    break;
                }
            }
        }
        Rules {
            id: myRules
            anchors.top: myScore.bottom
            anchors.topMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.right: parent.right
            anchors.rightMargin: 5
            visible: false

            onCancel: {
                myRoot.state = "";
            }
        }
        Synchro {
            id: mySynchro
            anchors.top: myScore.bottom
            anchors.topMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.right: parent.right
            anchors.rightMargin: 5
            visible: false


            onCancel: {

                myRoot.state = "";
            }
        }
        Play {
            id: myPlay
            anchors.top: myScore.bottom
            anchors.topMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.right: parent.right
            anchors.rightMargin: 5
            visible: false

            onCancel: {
                myRoot.state = "";
            }
        }
        Parameters {
            id: myParameters
            anchors.top: myScore.bottom
            anchors.topMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.right: parent.right
            anchors.rightMargin: 5
            visible: false

            onCancel: {
                myRoot.state = "";
            }
        }

        states: [
            State {
                name: ""
                PropertyChanges {
                    target: myStart
                    visible: true
                }
            },
            State {
                name: "Rules"
                PropertyChanges {
                    target: myStart
                    visible: false
                }
                PropertyChanges {
                    target: myRules
                    visible: true
                }
            },
            State {
                name: "Synchro"
                PropertyChanges {
                    target: myStart
                    visible: false
                }
                PropertyChanges {
                    target: mySynchro
                    visible: true
                }
            },
            State {
                name: "Play"
                PropertyChanges {
                    target: myStart
                    visible: false
                }
                PropertyChanges {
                    target: myPlay
                    visible: true
                }
            },
            State {
                name: "Game"
                PropertyChanges {
                    target: myStart
                    visible: false

                }
                PropertyChanges {
                    target: myPlay
                    visible: false
                }
                PropertyChanges {
                    target: myGame
                    visible: true
                }
            },
            State {
                name: "Parameters"
                PropertyChanges {
                    target: myStart
                    visible: false
                }
                PropertyChanges {
                    target: myParameters
                    visible: true
                }
            }
        ]
    }
}
