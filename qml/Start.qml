import QtQuick 2.2


Item {
    id: myRoot
    width: 480
    height: 590

    signal choice(int value);

    Column {
        id: colunm1
        spacing: myRoot.height / 30
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        JMButton {
            id: bRules
            width: myRoot.width - 2 * colunm1.spacing
            height: myRoot.height / 6
            text: qsTr("Règles du jeu");
             mouseArea.onClicked: myRoot.choice(1);
        }

        JMButton {
            id: bSynchro
            width: myRoot.width - 2 * colunm1.spacing
            height: myRoot.height / 6
            text: qsTr("Synchroniser");
             mouseArea.onClicked: myRoot.choice(2);
        }

        JMButton {
            id: bGame
            width: myRoot.width - 2 * colunm1.spacing
            height: myRoot.height / 6
            text: qsTr("Jouer");
             mouseArea.onClicked: myRoot.choice(3);
        }

        JMButton {
            id: bParameter
            width: myRoot.width - 2 * colunm1.spacing
            height: myRoot.height / 6
            text: qsTr("Paramètres");
             mouseArea.onClicked: myRoot.choice(4);
        }
    }
    BottomBar {
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        cancelText: qsTr("Quitter");

        onCancel:myRoot.choice(-1);
    }
}
