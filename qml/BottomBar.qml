import QtQuick 2.0

Item {
    id: myRoot
    width: 480
    height: 60
    property alias cancelText: bCancel.text

    signal cancel();

    JMButton {
        id: bCancel
        width: 200
        text: qsTr("Retour")

        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 10

        mouseArea.onClicked: cancel();
    }

}
